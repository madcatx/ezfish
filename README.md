# EZFish

A simple library to load detector traces produced by [EZChrom](https://www.agilent.com/en/product/software-informatics/analytical-software-suite/chromatography-data-systems/openlab-ezchrom) or 32Karat data acquisition software.

EZFish is written in [Rust](https://www.rust-lang.org) with C API. See either `include/ezfish.h`, `src/lib.rs` or `src/test_tool/ezf_test_tool.rs` for details how to call the API.

#### EZFish depends on

 - [explode](https://github.com/agrif/explode.git) Library for decompression of PKWARE ZIP streams (by Aaron Griffith)
 - [rust_compound_file](https://gitlab.com/madcatx/rust_compound_file) Library to read [Microsoft Compound files](https://learn.microsoft.com/en-us/cpp/mfc/containers-compound-files)

#### Installation

If you are using an older version of git, run `git submodule update --init --recursive` to fetch the dependencies

To just build the library the Rust way, simply run

    cargo build --release

Binaries will be built in the `target/release` directory.

##### Install the binary with C interface

If you intend to use the library in another project, you can use the provided CMakeLists to build and install a binary that exposes a C interface. To do so, run

    mkdir build
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=/some/where
    make
    make install

This will install the `include/ezfish.h` header file and `libezfish.so` (name may vary based on the target platform) binary the way libraries are usually installed. Note that the CMakeLists recipe needs to run Rust's `cargo` tool. Make sure that `cargo` is in your PATH, otherwise the build will fail. If you do not specify `CMAKE_INSTALL_PREFIX`, the default installation prefix will be used instead.
