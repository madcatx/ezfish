#ifndef EZFISH_H
#define EZFISH_H

#ifdef WIN32
    #define EZFISH_API __declspec(dllimport)
    #define EZFISH_CC __stdcall
#else
    #if defined(__GNUC__) || defined(__clang__)
        #ifdef __i386__
            #define EZFISH_CC __attribute__((__cdecl__))
        #else
            #define EZFISH_CC
        #endif
    #else
        #define EZFISH_CC
    #endif

    #define EZFISH_API
#endif

enum EzfResult {
    Success,                   /*!< Operation finished successfully */
    BadApiCall,                /*!< API function was called with improper parameters */
    BadData,                   /*!< Data file contains bad data */
    CompoundFileError,         /*!< Malformed compound file */
    CannotReadContainedFile,   /*!< Cannot read file contained in the compound file */
    DataTooLarge,              /*!< Data is too large to process */
    DecompressionError,        /*!< Cannot decompress detector data */
    MissingData,               /*!< Compound file does not contain expected data. It probably was not produced by EZChrom/32Karat software */
};

struct EzfScan {
    double x;
    double y;
};

/*! Single detector trace */
struct EzfDetectorTrace {
    const EzfScan *scans;         /*!< Detector scans */
    unsigned long long n_scans;   /*!< Size of the <tt>scans</tt> array */
    const char *x_units;          /*!< Units of X axis */
    const char *y_units;          /*!< Units of Y axis */
    const char *name;             /*!< Name of the detector trace */
};

/*! Detector traces */
struct EzfDetectorTraces {
    const EzfDetectorTrace *traces;   /*!< Detector traces */
    unsigned long long n_traces;      /*!< Size of the <tt>traces</tt> array */
};

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

EZFISH_API EzfDetectorTraces EZFISH_CC ezf_empty_traces();
EZFISH_API const char * EZFISH_CC ezf_error_to_string(EzfResult res);
EZFISH_API EzfResult EZFISH_CC ezf_read(const unsigned char *data, unsigned long long size, EzfDetectorTraces *traces);
EZFISH_API void EZFISH_CC ezf_release_traces(EzfDetectorTraces *traces);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* EZFISH_H */
