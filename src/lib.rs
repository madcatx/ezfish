use explode;
use rust_compound_file::*;

use std::ffi::{CStr, CString, c_char, c_ulonglong};
use std::str::FromStr;

/// Return codes of public API functions
#[repr(C)]
#[derive(PartialEq)]
pub enum EzfResult {
    /// Operation finished successfully
    Success,
    /// API function was called with improper parameters
    BadApiCall,
    /// Data file contains bad data
    BadData,
    /// Malformed compound file
    CompoundFileError,
    /// Cannot read file contained in the compound file
    CannotReadContainedFile,
    /// Data is too large to process
    DataTooLarge,
    /// Cannot decompress detector data
    DecompressionError,
    /// Compound file does not contain expected data. It probably was not produced by
    /// EZChrom/32Karat software
    MissingData,
}

/// Single detector scan
#[repr(C)]
pub struct EzfScan {
    pub x: f64,
    pub y: f64,
}

/// Single detector trace
#[repr(C)]
pub struct EzfDetectorTrace {
    /// Detector scans
    pub scans: *mut EzfScan,
    /// Size of the `scans` array
    pub n_scans: c_ulonglong,
    /// Units of X axis
    pub x_units: *mut c_char,
    /// Units of Y axis
    pub y_units: *mut c_char,
    /// Name of the detector trace
    pub name: *mut c_char,
}


/// Detector traces
#[repr(C)]
pub struct EzfDetectorTraces {
    /// Detector traces
    pub traces: *mut EzfDetectorTrace,
    /// Size of the `traces` array
    pub n_traces: c_ulonglong,
}

struct DetectorTrace {
    scans: Vec<EzfScan>,
    x_units: String,
    y_units: String,
    name: String,
}

struct DetectorTraceHandler {
    id: i32,
    name: String,
    x_scale: f32,
    y_units: String,
    y_scale: f32,
    detection_device_name: String,
    detector_units: String,
    sampling_period: String,
}

struct RawDetectorTrace {
    name: String,
    scans: Vec<i32>,
}

trait Advanceable {
    fn adv(self: Self, by: usize) -> Result<Self, EzfResult> where Self: Sized;
}

impl Advanceable for &[u8] {
    fn adv(self: Self, by: usize) -> Result<Self, EzfResult> {
        if self.len() < by {
            Err(EzfResult::BadData)
        } else {
            Ok(&self[by..])
        }
    }
}

static EZF_RES_SUCCESS: &'static str = concat!("Operation finished successfully", '\0');
static EZF_RES_BAD_API_CALL: &'static str = concat!("Bad API call", '\0');
static EZF_RES_BAD_DATA: &'static str = concat!("Data file appears to be corrupted", '\0');
static EZF_RES_COMPOUND_FILE_ERROR: &'static str = concat!("Data file cannot not be read as a compound file", '\0');
static EZF_RES_CANNOT_READ_CONTAINED_FILE: &'static str = concat!("Cannot read a file packed in the compound file", '\0');
static EZF_RES_DATA_TOO_LARGE: &'static str = concat!("Data block is too large to handle", '\0');
static EZF_RES_DECOMPRESSION_ERROR: &'static str = concat!("Cannot decompress detector data", '\0');
static EZF_RES_MISSING_DATA: &'static str = concat!("Compound file does not contain the expected data. It probably was not produced by EZChrom/32Karat software", '\0');
static EZF_RES__UNKNOWN_ERROR: &'static str = concat!("Unknown error", '\0');

static DETECTOR_DATA_DIR: &'static str = "Detector Data";
static DETECTOR_TRACE_HANDLER_FILE: &'static str = "Detector Trace Handler";
static DETECTOR_DATA_PATH: &'static str = "/Detector Data/";
static DETECTOR_TRACE_HANDLER_PATH: &'static str = "/Detector Trace Handler";

fn bytes_to_f32(buffer: &[u8]) -> Result<f32, EzfResult> {
    if buffer.len() < 4 {
        return Err(EzfResult::BadData);
    }

    match buffer[0..4].try_into() {
        Ok(bytes) => Ok(f32::from_le_bytes(bytes)),
        Err(_) => return Err(EzfResult::BadData),
    }
}

fn bytes_to_i32(buffer: &[u8]) -> Result<i32, EzfResult> {
    if buffer.len() < 4 {
        return Err(EzfResult::BadData);
    }

    match buffer[0..4].try_into() {
        Ok(bytes) => Ok(i32::from_le_bytes(bytes)),
        Err(_) => return Err(EzfResult::BadData),
    }
}

fn bytes_to_u16(buffer: &[u8]) -> Result<u16, EzfResult> {
    if buffer.len() < 2 {
        return Err(EzfResult::BadData);
    }

    match buffer[0..2].try_into() {
        Ok(bytes) => Ok(u16::from_le_bytes(bytes)),
        Err(_) => return Err(EzfResult::BadData),
    }
}

fn decompress_trace(buffer: &[u8]) -> Result<Vec<i32>, EzfResult> {
    if buffer.len() < 27 {
        return Err(EzfResult::DecompressionError);
    }

    let num_scans: i32 = bytes_to_i32(&buffer[4..8])?;
    let num_bytes: i32 = bytes_to_i32(&buffer[8..12])?;

    if num_scans < 0 {
        return Err(EzfResult::BadData);
    }
    if num_bytes < 1 || num_scans == 0 {
        return Ok(Vec::new());
    }

    let compressed_stream = gather_compressed_stream(&buffer[20..])?;
    let decompressed_stream = match explode::explode(&compressed_stream) {
        Ok(dec) => dec,
        Err(_) => return Err(EzfResult::DecompressionError),
    };

    let mut scans = Vec::<i32>::with_capacity(num_scans as usize);
    for idx in 0..num_scans {
        let off = (idx * 4) as usize;
        if decompressed_stream.len() < off + 4 {
            break;
        }

        let bytes = &decompressed_stream[off..off+4];
        let value = bytes_to_i32(bytes)?;
        scans.push(value);
    }

    Ok(scans)
}

fn gather_compressed_stream(mut buffer: &[u8]) -> Result<Vec<u8>, EzfResult> {
    let mut compressed = Vec::<u8>::new();

    if buffer.len() < 2 {
        return Err(EzfResult::BadData);
    }

    let mut block_size = bytes_to_u16(&buffer[0..2])? as usize;
    buffer = buffer.adv(2)?;
    while block_size > 0 {
        if buffer.len() < block_size {
            return Err(EzfResult::BadData);
        }
        compressed.extend(&buffer[0..block_size]);
        buffer = buffer.adv(block_size)?;

        if buffer.len() < 2 {
            return Err(EzfResult::BadData);
        }

        block_size = bytes_to_u16(&buffer[0..2])? as usize;
        buffer = buffer.adv(2)?;
    }

    Ok(compressed)
}

fn get_all_detector_traces(cpf: &CpfCompoundFile) -> Result<Vec<RawDetectorTrace>, EzfResult> {
    let files = get_files(cpf, DETECTOR_DATA_PATH)?;

    let mut ddata = Vec::<RawDetectorTrace>::with_capacity(files.len());
    for file in files {
        let file_path = format!("{}{}", DETECTOR_DATA_PATH, file);
        let buffer = read_file(cpf, &file_path)?;

        match decompress_trace(&buffer) {
            Ok(scans) => ddata.push(RawDetectorTrace{ name: file, scans }),
            Err(_) => (),
        }
    }

    Ok(ddata)
}

fn get_all_detector_trace_handlers(cpf: &CpfCompoundFile) -> Result<Vec<DetectorTraceHandler>, EzfResult> {
    let buffer = read_file(cpf, DETECTOR_TRACE_HANDLER_PATH)?;

    if buffer.len() < 28 {
        return Err(EzfResult::BadData);
    }

    let mut rest_of_buffer = &buffer[26..];
    let (cls_name, buf_shift) = pascal_string_to_latin1(rest_of_buffer)?;
    if cls_name != "CDetTraceInfo" {
        return Err(EzfResult::BadData);
    }
    rest_of_buffer = rest_of_buffer.adv(buf_shift)?;

    let mut handlers = Vec::<DetectorTraceHandler>::new();
    let mut try_next = true;
    while try_next {
        let (handler, next, _buffer) = read_detector_trace_info(rest_of_buffer)?;
        handlers.push(handler);
        try_next = next;
        rest_of_buffer = _buffer;
    }

    Ok(handlers)
}

fn get_files(cpf: &CpfCompoundFile, path: &str) -> Result<Vec<String>, EzfResult> {
    let c_path = CString::new(path).unwrap();

    let mut list_of_files = cpf_empty_string_list();
    let res = cpf_list_files(cpf, c_path.as_ptr(), &mut list_of_files);
    if res != CpfResult::Success {
        return Err(EzfResult::CompoundFileError);
    }

    let file_names = string_list_to_vec(list_of_files)?;
    let mut files = Vec::<String>::with_capacity(file_names.len());
    for fname in file_names {
        let file_path = format!("{}{}", path, fname);
        let c_file_path = CString::new(file_path).unwrap();

        let mut file_size: c_ulonglong = 0;
        if cpf_file_size(cpf, c_file_path.as_ptr(), &mut file_size) != CpfResult::Success {
            return Err(EzfResult::CannotReadContainedFile);
        }

        files.push(fname);
    }

    Ok(files)
}

fn get_subdirectories(cpf: &CpfCompoundFile, path: &str) -> Result<Vec<String>, EzfResult> {
    let c_path = CString::new(path).unwrap();

    let mut list_of_subdirs = cpf_empty_string_list();
    let res = cpf_list_subdirectories(cpf, c_path.as_ptr(), &mut list_of_subdirs);
    if res != CpfResult::Success {
        return Err(EzfResult::CompoundFileError);
    }

    string_list_to_vec(list_of_subdirs)
}

fn has_required_entries(cpf: &CpfCompoundFile) -> Result<(), EzfResult> {
    let subdirs_in_root = get_subdirectories(&cpf, "/")?;

    if !subdirs_in_root.into_iter().any(|subdir| subdir == DETECTOR_DATA_DIR) {
        Err(EzfResult::MissingData)
    } else {
        let files_in_root = get_files(cpf, "/")?;
        if !files_in_root.into_iter().any(|f| f == DETECTOR_TRACE_HANDLER_FILE) {
            Err(EzfResult::MissingData)
        } else {
            Ok(())
        }
    }
}

fn pascal_string_to_latin1(buffer: &[u8]) -> Result<(String, usize), EzfResult> {
    let length = bytes_to_u16(&buffer[0..2])? as usize;
    if buffer.len() < 2 + length {
        return Err(EzfResult::BadData);
    }

    let s = (&buffer[2..2+length]).into_iter().map(|byte| *byte as char).collect();
    Ok((s, length + 2))
}

fn pascal_short_string_to_latin1(buffer: &[u8]) -> Result<(String, usize), EzfResult> {
    if buffer.is_empty() {
        return Err(EzfResult::BadData);
    }

    let length = buffer[0] as usize;
    if buffer.len() < 1 + length {
        return Err(EzfResult::BadData);
    }

    let s = (&buffer[1..1+length]).into_iter().map(|byte| *byte as char).collect();
    Ok((s, length + 1))
}

fn read_file(cpf: &CpfCompoundFile, path: &str) -> Result<Vec<u8>, EzfResult> {
    let c_path = CString::new(path).unwrap();

    let mut file_size: c_ulonglong = 0;
    if cpf_file_size(cpf, c_path.as_ptr(), &mut file_size) != CpfResult::Success {
        return Err(EzfResult::CannotReadContainedFile);
    }
    let file_usize: usize = match file_size.try_into() {
        Ok(sz) => sz,
        Err(_) => return Err(EzfResult::DataTooLarge),
    };

    let mut buffer: Vec<u8> = vec![0; file_usize];
    let mut content = CpfContent {
        data: buffer.as_mut_ptr(),
        size: file_size,
    };

    if cpf_file_content(cpf, c_path.as_ptr(), &mut content) != CpfResult::Success {
        return Err(EzfResult::CannotReadContainedFile);
    }

    Ok(buffer)
}

fn read_detector_traces(cpf: &CpfCompoundFile) -> Result<Vec<DetectorTrace>, EzfResult> {
    let raw_traces = get_all_detector_traces(cpf)?;
    let handlers = get_all_detector_trace_handlers(cpf)?;

    let mut traces = Vec::<DetectorTrace>::new();
    for h in handlers {
        if let Some(raw_trace) = raw_traces.iter().find(|t| {
            let name_toks: Vec<&str> = t.name.split(" ").collect();
            if name_toks.len() < 2 {
                return false;
            }

            let id: i32 = match name_toks[1].parse() {
                Ok(id) => id,
                Err(_) => return false,
            };

            id == h.id
        }) {
            let mut x = 0.0_f64;
            let mut scans = Vec::<EzfScan>::with_capacity(raw_trace.scans.len());
            let x_step_mins = (h.x_scale / 60.0) as f64;

            for v in &raw_trace.scans {
                scans.push(EzfScan{x, y: (*v as f64) * (h.y_scale as f64)});
                x += x_step_mins;
            }

            traces.push(DetectorTrace{
                scans,
                x_units: h.sampling_period,
                y_units: h.y_units,
                name: h.name,
            })
        }
    }

    Ok(traces)
}

fn read_detector_trace_info(mut buffer: &[u8]) -> Result<(DetectorTraceHandler, bool, &[u8]), EzfResult> {
    if buffer.len() < 12 {
        return Err(EzfResult::BadData);
    }
    buffer = buffer.adv(12)?;

    let id = bytes_to_i32(buffer)?;
    buffer = buffer.adv(4)?;
    buffer = buffer.adv(4)?; // Skip uninteresting bytes

    let (name, buf_shift) = pascal_short_string_to_latin1(buffer)?;
    buffer = buffer.adv(buf_shift)?;

    let x_scale = bytes_to_f32(buffer)?;
    buffer = buffer.adv(4)?;

    let (y_units, buf_shift) = pascal_short_string_to_latin1(buffer)?;
    buffer = buffer.adv(buf_shift)?;

    let y_scale = bytes_to_f32(buffer)?;
    buffer = buffer.adv(4)?;

    let (sampling_period, buf_shift) = pascal_short_string_to_latin1(buffer)?;
    buffer = buffer.adv(buf_shift)?;
    buffer = buffer.adv(32)?; // Skip uninteresting bytes

    let (detection_device_name, buf_shift) = pascal_short_string_to_latin1(buffer)?;
    buffer = buffer.adv(buf_shift)?;

    let (detector_units, buf_shift) = pascal_short_string_to_latin1(buffer)?;
    buffer = buffer.adv(buf_shift)?;

    buffer = buffer.adv(8)?; // Skip uninteresting bytes

    // Skip uninteresting string
    let (_dummy, buf_shift) = pascal_short_string_to_latin1(buffer)?;
    buffer = buffer.adv(buf_shift)?;

    buffer = buffer.adv(24)?; // Skip uninteresting bytes

    let next = bytes_to_u16(buffer)?;
    buffer = buffer.adv(2)?;

    Ok((
        DetectorTraceHandler{
            id,
            name,
            x_scale,
            y_units,
            y_scale,
            detection_device_name,
            detector_units,
            sampling_period,
        },
        next == 0x8001_u16,
        buffer
    ))

}

fn string_list_to_vec(mut list: CpfStringList) -> Result<Vec<String>, EzfResult> {
    let list_size: usize = match list.size.try_into() {
        Ok(sz) => sz,
        Err(_) => {
            cpf_release_string_list(&mut list);

            return Err(EzfResult::DataTooLarge);
        },
    };

    let mut vec = Vec::<String>::with_capacity(list.size as usize);
    let wrapped_list: Vec<*mut c_char> = unsafe { Vec::from_raw_parts(list.strings, list_size, list_size) };
    for c_s in &wrapped_list {
        let s = unsafe { CStr::from_ptr(*c_s) };
        vec.push(String::from_str(s.to_str().unwrap()).unwrap());
    }

    std::mem::forget(wrapped_list);
    cpf_release_string_list(&mut list);

    Ok(vec)
}

fn traces_to_c_traces(traces: Vec<DetectorTrace>) -> Vec<EzfDetectorTrace> {
    let mut c_traces = Vec::<EzfDetectorTrace>::new();

    for mut t in traces {
        t.scans.shrink_to_fit(); // Necessary so we do not leak the excess bytes

        let c_x_units = CString::new(t.x_units).unwrap();
        let c_y_units = CString::new(t.y_units).unwrap();
        let c_name = CString::new(t.name).unwrap();

        c_traces.push(EzfDetectorTrace{
            scans: t.scans.as_mut_ptr(),
            n_scans: t.scans.len() as c_ulonglong,
            x_units: CString::into_raw(c_x_units),
            y_units: CString::into_raw(c_y_units),
            name: CString::into_raw(c_name),
        });

        std::mem::forget(t.scans);
    }

    c_traces.shrink_to_fit();
    c_traces
}

/// Creates empty `EzfDetectorTraces` object
#[no_mangle]
pub extern "system" fn ezf_empty_traces() -> EzfDetectorTraces {
    EzfDetectorTraces{
        traces: std::ptr::null_mut(),
        n_traces: 0,
    }
}

/// Translates `EzfResult` to error message
///
/// Arguments:
///
/// * `res`: Return code to translate
#[no_mangle]
pub extern "system" fn ezf_error_to_string(res: EzfResult) -> *const c_char {
    let s = match res {
        EzfResult::Success => EZF_RES_SUCCESS,
        EzfResult::BadApiCall => EZF_RES_BAD_API_CALL,
        EzfResult::BadData => EZF_RES_BAD_DATA,
        EzfResult::CompoundFileError => EZF_RES_COMPOUND_FILE_ERROR,
        EzfResult::CannotReadContainedFile => EZF_RES_CANNOT_READ_CONTAINED_FILE,
        EzfResult::DataTooLarge => EZF_RES_DATA_TOO_LARGE,
        EzfResult::DecompressionError => EZF_RES_DECOMPRESSION_ERROR,
        EzfResult::MissingData => EZF_RES_MISSING_DATA,
        _ => EZF_RES__UNKNOWN_ERROR,
    };

    s.as_ptr() as *const c_char
}

/// Reads detector traces from a compound file
///
/// Arguments:
///
/// * `data`: Compound file as array of bytes
/// * `size`: Size of the `data` array
/// * `traces`: Pointer to `EzfDetectorTraces` object that will contain the detector traces. Passed
///             object must be empty. The object will not be modified unless the detector traces
///             are read successfully.
#[no_mangle]
pub extern "system" fn ezf_read(data: *const u8, size: c_ulonglong, traces: *mut EzfDetectorTraces) -> EzfResult {
    unsafe {
        if (*traces).traces != std::ptr::null_mut() || (*traces).n_traces != 0 {
            return EzfResult::BadApiCall;
        }
    }

    let mut cpf = cpf_empty_compound_file();
    let res = cpf_read(data, size as usize, &mut cpf);
    if res != CpfResult::Success {
        return EzfResult::CompoundFileError;
    }

    if let Err(_) = has_required_entries(&cpf) {
        cpf_release_compound_file(&mut cpf);

        return EzfResult::MissingData;
    }

    match read_detector_traces(&cpf) {
        Ok(_traces) => {
            cpf_release_compound_file(&mut cpf);

            let mut c_traces = traces_to_c_traces(_traces);

            unsafe {
                (*traces).traces = c_traces.as_mut_ptr();
                (*traces).n_traces = c_traces.len() as c_ulonglong;
            }

            std::mem::forget(c_traces);

            EzfResult::Success
        },
        Err(e) => {
            cpf_release_compound_file(&mut cpf);

            e
        }
    }
}

/// Releases resouces used by `EzfDetectorTraces`
///
/// Arguments:
///
/// * `traces`: Object to release
#[no_mangle]
pub extern "system" fn ezf_release_traces(traces: *mut EzfDetectorTraces) {
    unsafe {
        if (*traces).traces != std::ptr::null_mut() {
            let mut _traces = Vec::<EzfDetectorTrace>::from_raw_parts((*traces).traces, (*traces).n_traces as usize, (*traces).n_traces as usize);

            for t in _traces {
                drop(Vec::<EzfScan>::from_raw_parts(t.scans, t.n_scans as usize, t.n_scans as usize));

                drop(CString::from_raw(t.x_units));
                drop(CString::from_raw(t.y_units));
                drop(CString::from_raw(t.name));
            }
        }
    }
}
