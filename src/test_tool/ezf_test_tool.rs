use ezfish::*;

use std::ffi::{CStr, c_char, c_ulonglong};
use std::fs::File;
use std::io::prelude::*;

fn c_str_to_str(c_str: *mut c_char) -> String {
    let cc_str = unsafe { CStr::from_ptr(c_str) };

    String::from(cc_str.to_str().unwrap())
}

fn err_to_str(res: EzfResult) -> String {
    let c_str = unsafe {
        CStr::from_ptr(ezf_error_to_string(res))
    };

    String::from(c_str.to_str().unwrap())
}

fn main() {
    let file_path;

    let args: Vec<String> = std::env::args().collect();
    match args.len() {
        2 => file_path = args[1].clone(),
        _ => panic!("Invalid arguments"),
    };

    let mut fh = match File::open(file_path) {
        Ok(fh) => fh,
        Err(e) => panic!("Cannot open compound file: {}", e),
    };

    let mut buffer = Vec::<u8>::new();
    if let Err(e) = fh.read_to_end(&mut buffer) {
        panic!("Cannot read data file: {}", e);
    }

    let mut traces = ezf_empty_traces();
    let res = ezf_read(buffer.as_ptr(), buffer.len() as c_ulonglong, &mut traces);
    if res != EzfResult::Success {
        panic!("Cannot read traces: {}", err_to_str(res));
    }

    let _traces = unsafe { Vec::<EzfDetectorTrace>::from_raw_parts(traces.traces, traces.n_traces as usize, traces.n_traces as usize) };

    for t in &_traces {
        println!("Name: {}", c_str_to_str(t.name));
        println!("{};{};", c_str_to_str(t.x_units), c_str_to_str(t.y_units));

        let scans =  unsafe { Vec::<EzfScan>::from_raw_parts(t.scans, t.n_scans as usize, t.n_scans as usize) };

        for scan in &scans {
            println!("{};{}", scan.x, scan.y);
        }

        std::mem::forget(scans);
    }

    std::mem::forget(_traces);
    ezf_release_traces(&mut traces);
}
